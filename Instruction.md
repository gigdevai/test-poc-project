## To-Do List Application Documentation

### Introduction

The To-Do List Application is a web-based tool designed to help users manage their tasks and stay organized. This documentation provides an overview of the project, as well as instructions for how to download and start doing a test project.

### Getting Started

To start test project, you will need to download the latest version from the project repository.

The application is built using React, so you will need a command line and a code editor to work with the project.
We recommend using **Visual Studio Code (VSC)**, which is a free code editor that includes a command line terminal.
Once you have downloaded the project, you can open it in VSC and begin working with the code.
Then, in the terminal **(CTRL+`)**, install all the necessary npm packages using the command line:

`npm install --force`

After successfully downloading and installing the packages (don't worry about the warnings), you can launch Storybook by entering the command:

`npm run storybook`

### Storybook user Interface

In the default browser, you will see the Storybook project that has been launched locally on your computer.

The Storybook itself is divided into three main parts.
On the left is a list of components along with a sub-list of stories.
On the top right, we have an area that shows the currently selected component.
At the beginning, it will be an empty blue field. This is the part of the task which is implementing the components.

At the bottom right, there is a list of additional tools. The most useful one at the beginning will be the **"Design"** tab, which contains the definitions of the appearance of a given component in a given story. Additionally, there is an **"Interactions"** tab that shows interactive tests for selected stories.

In our task, we have defined one interactive test for **"InboxScreen->Pin Task"**.

### Test project for implementation

Let's focus on the task and what needs to be done. On the left panel, we have a list of components to implement:

`Task` - the basic element of a task list.
It has four main stories: default, pinned, archived, and with long text.

`TaskList` - a composition of a task list.
This component contains four stories: a default list with multiple tasks, a loading state, an empty list, and a list with a pinned task.

`InboxScreen` - the final endpoint component of the to-do list application.
It has a default state and what it looks like when we fail to load the task list. Additionally, it has an interactive test for "Pin Task".

All stories and the component skeletons can be found in the directory: `/src/` and `/src/components/`.
The stories are described in the `stories.js` files.

### Automatic visual tests

Additional tests that you can run are visual tests of what you are implementing with the previously specified design.
These tests renders your components and compare them with reference images in the `/src/image_snapshots/` directory.
To run these tests, use the command:

`npm run test`

then choose to run all the tests.
After running all the tests, you will be able to see the differences between the current appearance of the component and what is different in the generated images in the `/src/image_snapshots/diff_output/` directory.
The images consist (from left to right) of the origin expected result, the difference, and what the component currently implements.

### Watch mode

Watch mode.
This is the default mode of operation for tests and Storybook.
This means you don't have to repeat the above commands after changing the code.
Everything will be reloaded and refreshed automatically.
